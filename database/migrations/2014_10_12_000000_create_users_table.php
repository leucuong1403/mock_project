<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role');
            $table->foreign('role')->references('id')->on('roles');
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('users')->insert([
            [
                "role" => 1,
                "name" =>"Quang Bình",
                "email" => "admin@gmail.com",
                "phone" => "0967019299",
                "password"=> Hash::make("12345678")
            ],
            [
                "role" => 2,
                "name"=>"Bình hn",
                "email"=> "nvdonhang@gmail.com",
                "phone" => "0967019200",
                "password"=> Hash::make("12345678")
            ],
            [
                "role" => 3,
                "name"=>"Bình kt",
                "email"=> "nvkho@gmail.com",
                "phone" => "0967019289",
                "password"=> Hash::make("12345678")
            ],
            [
                "role" => 4,
                "name"=>"Bình nh",
                "email"=> "nvbaiviet@gmail.com",
                "phone" => "0967019298",
                "password"=> Hash::make("12345678")
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};

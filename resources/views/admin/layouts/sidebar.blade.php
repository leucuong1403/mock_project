  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          {{-- <img src="{{Auth::user()->avatar}}" class="img-circle elevation-2" alt="User Image"> --}}
        </div>
        <div class="info">
          <a href="#" class="d-block">{{  Auth::user()->name}}</a>
        </div>
      </div>
      <?php
        $routeName = Route::currentRouteName();
      ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          @if (Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ route('home') }}" class="@if ($routeName == 'home')nav-link active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
                Tổng quan
            </a>
          </li>
            
            <li class="nav-item">
              <a href="{{ route('category.index') }}" class="@if ($routeName == 'category.index')nav-link active @endif">
                <i class="nav-icon fas fa-tree"></i>
                  QL Danh mục SP
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('product.index') }}" class="@if ($routeName == 'product.index')nav-link active @endif">
                <i class="nav-icon fas fa-archive"></i>
                  Quản lý sản phẩm
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('order.index') }}" class="@if ($routeName == 'order.index')nav-link active @endif">
                <i class="nav-icon fas fa-cart-plus"></i>
                  Quản lý đơn hàng
              </a>
            </li>
            
            <li class="nav-item">
              <a href="{{ route('user.index') }}" class="@if ($routeName == 'user.index')nav-link active @endif">
                <i class="nav-icon fas fa-user"></i>
                  Quản lý tài khoản
              </a>
            </li>
          @endif
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

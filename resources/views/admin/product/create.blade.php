@extends('admin.layouts.app-new')
@section('title-page', 'Thêm mới sản phẩm')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 ">
                            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 ">
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Tên sản phẩm</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input name="name" placeholder="Tên sản phẩm" value=""
                                                    type="text" autocomplete="off" class="form-control" required>
                                                @if ($errors->has('name'))
                                                    <span
                                                        class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Mã sản phẩm</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input name="code" placeholder="Mã sản phẩm" value=""
                                                    type="text" autocomplete="off" class="form-control" required>
                                                @if ($errors->has('code'))
                                                    <span
                                                        class="help-block text-danger "><strong>{{ $errors->first('code') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Giá</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input type="text" placeholder="Nhập giá" value=""
                                                    autocomplete="off" title="Nhập giá" name="price" class="form-control"
                                                    required>
                                                @if ($errors->has('price'))
                                                    <span
                                                        class="help-block text-danger "><strong>{{ $errors->first('price') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Danh mục SP</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <select class="form-control" id="category_id" name="category_id">
                                                    <option value="">--Chọn danh mục sp--</option>
                                                    @foreach ($category as $c)
                                                        <option value="{{ $c->id }}">
                                                            {{ $c->name . ' - ' . $c->parent_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Số lượng</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input type="text" placeholder="Nhập số lượng" value=""
                                                    autocomplete="off" title="Nhập số lượng" name="quantity"
                                                    class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Hình ảnh </label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input type="file" required accept="image/*" name="image[]" multiple class="input_file">

                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">% sale</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <input type="text" placeholder="Nhập %" value="" autocomplete="off"
                                                    title="Nhập %" name="percent_sale" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Trạng thái HOT</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <select class="form-control" id="is_hot" name="is_hot">
                                                    <option value="0" selected>Không HOT</option>
                                                    <option value="1">HOT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <label class="">Trạng thái sale</label>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7">
                                                <select class="form-control" id="is_sale" name="is_sale">
                                                    <option value="0" selected>Không Sale</option>
                                                    <option value="1">Sale</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mt-2">
                                        <label for="">Mô tả</label>
                                        <textarea placeholder="Nhập mô tả" rows="5" cols="80" name="description" id="ckeditor"
                                            class="form-control"></textarea>
                                        @if ($errors->has('description'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('description') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                        <a href="{{ route('product.index') }}" class="mt-2 btn btn-danger text-white"><i
                                                class="fa fa-arrow-circle-o-left"></i>
                                            Trở về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.layouts.app-new')
@section('title-page', 'Cập nhật đơn hàng' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('order.edit',$order->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Tên Khách hàng</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                        <input name="name" placeholder="Tên khách hàng" value="{{$order->name}}" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <label class="">Số điện thoại</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="text"  placeholder="Nhập SĐT" value="{{$order->phone}}" autocomplete="off"  title="Nhập SĐT" name="phone" class="form-control"  required>
                                                @if ($errors->has('phone'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('phone') }}</strong></span>
                                                @endif
                                            </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Địa chỉ</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập địa chỉ" value="{{$order->address}}" autocomplete="off"  title="Nhập địa chỉ" name="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">email</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập email" value="{{$order->email}}" autocomplete="off"  title="Nhập email" name="email" class="form-control" >
                                            @if ($errors->has('email'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Sản phẩm</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <?php 
                                                
                                                foreach($order->product_order as $ls){
                                                    $arr[]  = $ls->product_id;
                                                }
                                            ?>
                                            <select multiple  class="form-control select2" id="product_id"
                                                name="product_id[]" >
                                                <option value="">--Chọn sản phẩm--</option>
                                                @foreach($product as $p)
                                                        <option value="{{$p->id}}"  {{in_array($p->id, $arr)? 'selected' : ''}}
                                                        >{{$p->name.'- Mã:'.$p->code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if($order->type_buy == 1)
                                        <div class="row mt-2">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <label class="">Trạng thái đơn hàng</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <select class="form-control" id="delivery_status" name="delivery_status">
                                                    <option value="0" @if($order->delivery_status == 0) selected @endif>Chờ xác nhận</option>
                                                    <option value="1" @if($order->delivery_status == 1) selected @endif>Đã xác nhận</option>
                                                    <option value="2" @if($order->delivery_status == 2) selected @endif>Đang giao</option>
                                                    <option value="3" @if($order->delivery_status == 3) selected @endif>Đã giao</option>
                                                    <option value="4" @if($order->delivery_status == 4) selected @endif>Đã hủy từ khách hàng</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('order.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product_Order;


class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code',
        'name',
        'phone',
        'address',
        'email',
        'user_id',
        'total_money',
        'type_buy',
        'note',
        'status',
        'delivery_status',
        'created_at'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function product_order()
    {
        return $this->hasMany(Product_Order::class);
    }
}

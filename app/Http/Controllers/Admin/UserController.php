<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\EditUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $listUser = User::with('roles')->where('status', 1);
        if ($request->name) {
            $listUser = $listUser->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->phone) {
            $listUser = $listUser->where('phone', 'like', '%' . $request->phone . '%');
        }
        if ($request->role) {
            $listUser = $listUser->where('role', $request->role);
        }
        if ($request->from_date) {
            $listUser = $listUser->where('created_at', '>=', $request->from_date);
        }
        if ($request->to_date) {
            $listUser = $listUser->where('users.created_at', '<=', date('Y-m-d H:i:s', strtotime($request->to_date) + 24 * 3600 - 1));
        }
        $listUser = $listUser->paginate(6);
        $ls_role = Role::get();

        return view('admin.user.index', compact('listUser', 'ls_role'));
    }

    public function create()
    {
        $ls_role = Role::get();
        return view('admin.user.create', compact('ls_role'));
    }
    public function update($id)
    {
        $user = User::findorfail($id);
        $ls_role = Role::get();
        return view('admin.user.update', compact('ls_role', 'user'));
    }

    public function store(StoreUserRequest $request)
    {
        try {
            if ($request->input('role') == null) {
                $request->session()->flash('error', 'Vui lòng chọn loại tài khoản!');
                return redirect()->back();
            }
            $id = User::insertGetId([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'role' => $request->input('role'),
                'password' => Hash::make($request->input('password')),
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            $request->session()->flash('success', 'Thêm mới tài khoản thành công!');
            $url = redirect()->route('user.index')->getTargetUrl();
            return redirect($url);
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }
    public function edit(EditUserRequest $request, $id)
    {
        if ($request->input('role') == null) {
            $request->session()->flash('error', 'Vui lòng chọn loại tài khoản!');
            return redirect()->back();
        }
        $check_mail = User::where([['id', '<>', $id], ['email', $request->input('email')], ['status', 1]])->first();
        if ($check_mail != null) {
            $request->session()->flash('error', 'Email đã tồn tại!');
            return redirect()->back();
        }
        $check_phone = User::where([['id', '<>', $id], ['phone', $request->input('phone')], ['status', 1]])->first();
        if ($check_phone != null) {
            $request->session()->flash('error', 'Số điện thoại đã tồn tại!');
            return redirect()->back();
        }
        $user = User::findorfail($id);
        $user->update($request->all());
        $request->session()->flash('success', 'Cập nhật tài khoản thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }
    public function delete(Request $request, $id)
    {
        $delete = User::where('id', $id)->update([
            'status' => 0,
        ]);
        $request->session()->flash('success', 'Xóa tài khoản thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }

    public function resetPassword(Request $request, $id)
    {
        $delete = User::where('id', $id)->update([
            'password' => Hash::make("12345678"),
        ]);
        $request->session()->flash('success', 'Reset mật khẩu thành công!');
        $url = redirect()->route('user.index')->getTargetUrl();
        return redirect($url);
    }
}

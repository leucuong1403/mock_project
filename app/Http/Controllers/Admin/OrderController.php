<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product_Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $phone = $request->input('phone');
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->type_buy) {
            $where[] = ['type_buy', $request->type_buy];
        }
        if ($request->from_date) {
            $where[] = ['created_at', '>=', $request->from_date];
        }
        if ($request->to_date) {
            $where[] = ['created_at', '<=', date('Y-m-d H:i:s', strtotime($to_date) + 24 * 3600 - 1)];
        }
        $listOrder = Order::where($where)->with('user', 'product_order.product');
        if ($request->category_id) {
            $category_id = $request->category_id;
            $listOrderDetail = Product_Order::select('p.name', 'products_orders.order_id')
                ->leftjoin('products as p', 'p.id', 'products_orders.product_id')
                ->where('p.category_id', $category_id)->distinct('products_orders.order_id')->get();
            $arr = [];
            foreach ($listOrderDetail as $ls) {
                $arr[] = $ls->order_id;
            }
            $listOrder = $listOrder->whereIN('orders.id', $arr);
        }
        // $listOrder = $listOrder->get();
        $total_money = $listOrder->sum('orders.total_money');
        $listOrder = $listOrder->orderby('orders.id', 'desc')->paginate(12);
        $category = Category::all();
        return view('admin.order.index', compact('listOrder', 'category', 'total_money'));
    }

    public function create()
    {
        $category = Category::all();
        $product = Product::all();
        return view('admin.order.create', compact('category', 'product'));
    }

    public function update($id)
    {
        $order = Order::find($id)->load('user', 'product_order.product');
        $category = Category::all();
        $product = Product::all();
        return view('admin.order.update', compact('category', 'order', 'product'));
    }

    public function store(StoreOrderRequest $request)
    {
        try {
            if (!$request->product_id || count($request->product_id) == 0) {
                $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
                return redirect()->back();
            }
            $ls_product = Product::whereIn('id', $request->product_id)->get();
            $req_order = $request->all();
            $req_order['total_money'] = $ls_product->sum('price');
            $req_order['status'] = 1;
            $req_order['type_buy'] = 2;
            $req_order['delivery_status'] = 1;

            $order = Order::create($req_order);
            $total_money = 0;
            $price = 0;
            foreach ($ls_product as $ls) {
                $price = $ls->price;
                if ($ls->is_sale == 1) {
                    $price = $ls->price - $ls->price * $ls->percent_sale / 100;
                }
                $total_money += $price;
                $create = Product_Order::insert([
                    'order_id' => $order->id,
                    'product_id' => $ls->id,
                    'quantity' => 1,
                    'money' => $price,
                ]);
                Product::where('id', $ls->id)->update([
                    'quantity' => $ls->quantity - 1,
                ]);
            }
            $order->update([
                'total_money' => $total_money,
            ]);
            $request->session()->flash('success', 'Thêm mới đơn hàng thành công!');
            $url = redirect()->route('order.index')->getTargetUrl();
            return redirect($url);
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }

    public function edit(StoreOrderRequest $request, $id)
    {
        try {
            if ($request->product_id == null) {
                $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
                return redirect()->back();
            }
            $order = Order::findorfail($id);
            $ls_product = Product::whereIn('id', $request->product_id)->get();
            $req_order = $request->all();
            $req_order['total_money'] = $ls_product->sum('price');
            $order->update($req_order);
            $total_money = 0;
            $price = 0;
            if (count($ls_product) > 0) {
                $delete = Product_Order::where('order_id', $id)->delete();
                foreach ($ls_product as $ls) {
                    $price = $ls->price;
                    if ($ls->is_sale == 1) {
                        $price = $ls->price - $ls->price * $ls->percent_sale / 100;
                    }
                    $total_money += $price;
                    $create = Product_Order::insert([
                        'order_id' => $order->id,
                        'product_id' => $ls->id,
                        'quantity' => 1,
                        'money' => $price,
                    ]);
                    Product::where('id', $ls->id)->update([
                        'quantity' => $ls->quantity - 1,
                    ]);
                }
            }

            $request->session()->flash('success', 'Cập nhật đơn hàng thành công!');
            $url = redirect()->route('order.index')->getTargetUrl();
            return redirect($url);
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }

    public function delete(Request $request, $id)
    {
        $delete_product_order = Product_Order::where('order_id', $id)->delete();
        $delete = Order::where('id', $id)->delete();

        $request->session()->flash('success', 'Hủy đơn hàng thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }

    public function check_order(Request $request, $id)
    {
        $delete = Order::where('id', $id)->update([
            'status' => 1,
        ]);
        $request->session()->flash('success', 'Duyệt đơn hàng thành công!');
        $url = redirect()->route('order.index')->getTargetUrl();
        return redirect($url);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Auth\LoginRequest;

class HomeController extends Controller
{
    public function loginUser(LoginRequest $request)
    {
        try {
            $input = $request->only('email', 'password');
            if (Auth::attempt($input)) {
                $request->session()->flash('success', 'Đăng nhập thành công!');
                if (Auth::user()->role != 0) {
                    $url = redirect()->route('home')->getTargetUrl();
                    return redirect($url);
                } else {
                    return redirect()->back();
                }

            }
            $request->session()->flash('error', 'Email hoặc mật khẩu không chính xác!');
            return redirect()->back();
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('index');
    }
}

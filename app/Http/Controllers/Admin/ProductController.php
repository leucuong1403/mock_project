<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\EditProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Image_Product;
use App\Traits\UploadImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $listProduct = Product::select('products.*', 'c.name as category_name')
            ->leftjoin('category as c', 'c.id', 'products.category_id')
            ->where('products.status', 1);
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $listProduct = $listProduct->where('products.name', 'like', '%' . $name . '%');
        }
        if (!empty($request->input('is_sale'))) {
            $is_sale = $request->input('is_sale');
            $listProduct = $listProduct->where('products.is_sale', $is_sale);
        }
        if (!empty($request->input('is_hot'))) {
            $is_hot = $request->input('is_hot');
            $listProduct = $listProduct->where('products.is_hot', $is_hot);
        }
        if (!empty($request->input('category_id'))) {
            $category_id = $request->input('category_id');
            $listProduct = $listProduct->where('products.is_hot', $category_id);
        }
        if (!empty($request->input('from_date'))) {
            $from_date = $request->input('from_date');
            $listProduct = $listProduct->where('products.created_at', '>=', $from_date);
        }
        if (!empty($request->input('to_date'))) {
            $to_date = $request->input('to_date');
            $listProduct = $listProduct->where('products.created_at', '<=', date('Y-m-d H:i:s', strtotime($to_date) + 24 * 3600 - 1));
        }
        $listProduct = $listProduct->orderby('products.id', 'desc')->paginate(6);
        $category = Category::all();
        return view('admin.product.index', compact('listProduct', 'category'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin.product.create', compact('category'));
    }

    public function update($id)
    {
        $product = Product::findorfail($id);
        $image = Image_Product::where('product_id', $id)->get();
        $category = Category::all();
        return view('admin.product.update', compact('category', 'product', 'image'));
    }

    public function store(CreateProductRequest $request)
    {
        try {

            if ($request->input('category_id') == null) {
                $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
                return redirect()->back();
            }
            $req = $request->all();
            $create = Product::create($req);

            if ($request->file('image') != null) {

                foreach($request->file('image') as $file){
                    
                    $imageName=time().'_'.$file->getClientOriginalName();
                    $file->move(\public_path("/upload"),$imageName);
                    $create_img = Image_Product::insert([
                        'product_id' => $create->id,
                        'image' => $imageName,
                    ]);

                }
            }


           
            $request->session()->flash('success', 'Thêm mới sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }
    public function edit(EditProductRequest $request, $id)
    {
        try {

            if ($request->category_id == null) {
                $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
                return redirect()->back();
            }
            $req = $request->all();
            $product = Product::findorfail($id);
            $product->update($req);
            $delete = Image_Product::where('product_id', $id)->delete();
            if ($request->file('image') != null) {

                foreach($request->file('image') as $file){
                    
                    $imageName=time().'_'.$file->getClientOriginalName();
                    $file->move(\public_path("/upload"),$imageName);
                    $create_img = Image_Product::insert([
                        'product_id' => $id,
                        'image' => $imageName,
                    ]);
                }
            }
            $request->session()->flash('success', 'Cập nhật sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }

    public function delete(Request $request, $id)
    {
        $delete = Product::where('id', $id)->update([
            'status' => 0,
        ]);
        $request->session()->flash('success', 'Xóa sản phẩm thành công!');
        $url = redirect()->route('product.index')->getTargetUrl();
        return redirect($url);
    }

    public function update_hot(Request $request, $id)
    {
        try {
            $check = Product::where('id', $id)->first();
            if ($check->is_hot == 1) {
                $update = Product::where('id', $id)->update([
                    'is_hot' => 0,
                ]);
                $request->session()->flash('success', 'Tắt HOT sản phẩm thành công!');
                $url = redirect()->route('product.index')->getTargetUrl();
                return redirect($url);
            } else {
                $update = Product::where('id', $id)->update([
                    'is_hot' => 1,
                ]);
                $request->session()->flash('success', 'Bật HOT sản phẩm thành công!');
                $url = redirect()->route('product.index')->getTargetUrl();
                return redirect($url);
            }
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }
    public function update_sale(Request $request, $id)
    {
        try {
            $check = Product::where('id', $id)->first();
            if ($check->is_sale == 1) {
                $update = Product::where('id', $id)->update([
                    'is_sale' => 0,
                ]);
                $request->session()->flash('success', 'Tắt Sale sản phẩm thành công!');
                $url = redirect()->route('product.index')->getTargetUrl();
                return redirect($url);
            } else {
                $update = Product::where('id', $id)->update([
                    'is_sale' => 1,
                ]);
                $request->session()->flash('success', 'Bật Sale sản phẩm thành công!');
                $url = redirect()->route('product.index')->getTargetUrl();
                return redirect($url);
            }
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }

}

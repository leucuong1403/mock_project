<?php

namespace App\Http\Requests\User;
use Illuminate\Foundation\Http\FormRequest;


class StoreUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'phone'     => 'required|numeric|unique:users',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'name.required'     => "Vui lòng nhập tên danh mục",
            'phone.required'    => "Vui lòng nhập số điện thoại",
            'phone.numeric'     => "số điện thoại chỉ bao gồm chữ số",
            'phone.unique'      => "số điện thoại đã tồn tại",
            'email.required'    => 'Email không được để trống',
            'email.email'       => 'Email phải đúng định dạng',
            'email.unique'      => 'Email đã tồn tại',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min'      => 'Vui lòng nhập mật khẩu lớn hơn 6 ký tự',
        ];
    }
}

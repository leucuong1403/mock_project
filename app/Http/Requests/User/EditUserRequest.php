<?php

namespace App\Http\Requests\User;
use Illuminate\Foundation\Http\FormRequest;


class EditUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
                'address' => 'required',
        ];
    }
    public function messages()
    {
        return [
                'name.required' => 'Vui lòng nhập họ và tên',
                'phone.required' => 'Vui lòng nhập số điện thoại',
                'phone.numeric' => 'Vui lòng nhập đúng số điện thoại',
                'email.email' => 'Vui lòng nhập đúng định dạng mail',
                'address.required' => 'Vui lòng nhập địa chỉ',
        ];
    }
}
